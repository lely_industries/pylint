#!/usr/bin/env bash
#
# This script updates the package files used by the docker file `docker\Dockerfile`.
# Effectively pinning all dependencies used. This allows for reproducible docker images.
#
set -Eeuo pipefail
shopt -s extglob
cd -P -- "$(dirname -- "${BASH_SOURCE[0]}")"
cat /etc/os-release || cat /usr/lib/os-release

# python pip
while read -r REQUIREMENTS; do
    python3 -m venv --clear /tmp/.venv-autoupdate
    . /tmp/.venv-autoupdate/bin/activate
    pip install \
        --upgrade \
        pip \
        wheel
    pip install \
        --requirement "${REQUIREMENTS}" \
        --upgrade
    pip freeze | grep -v "pkg.resources" >|docker/"${REQUIREMENTS}"
    deactivate ""
done <<EOF
requirements.txt
EOF

# Cleanup
rm -rf /tmp/.venv-autoupdate

# ./docker/docker.sh -e -- ./autoupdate.sh
