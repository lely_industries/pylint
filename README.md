# PyLint

## `.pylintrc`

The [pylint] configuration is in the `[tool.pylint]` section of `pyproject.toml` (linked from the
precommit submodule) and can be overridden by using a `.pylintrc` file. Note that the ancient
version of [pylint] shipped with Ubuntu 20.04 LTS does not support `pyproject.toml` as a config
file.

## Template parameters for `.gitlab-ci.yml`

To use the template in your `.gitlab-ci.yml`, provide the parameters for [pylint] in the variable
`PYLINT_PARAMETERS`. Executing [pylint] without parameters will show the [pylint] help instead of
checking your code (which requires at least one module or package). See the documentation.

Example:

```yaml
variables:
    PYLINT_PARAMETERS: src
```

## Simplify dependencies

The supplied [pylint] image will not contain your project dependencies. This causes [pylint] to
produce issues. While you could extend the supplied image with these dependencies, it is much easier
if you use your project image that already contains these dependencies and add `pylint-gitlab` to
your dependencies.

Override the [pylint] job like this in your `.gitlab-ci.yml` file, providing your image and image
version:

```yaml
pylint:
    image: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    needs:
        - job: BuildImage
          artifacts: false
```

---

[pylint]: https://pylint.org/
